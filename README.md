## Fatal Staircase iOS/Android mobile game ##

[Unity AdMob manual](https://github.com/googleads/googleads-mobile-plugins/tree/master/unity)


### Xcode hints ###
* Build Settings -> Search paths -> Library search path - add this shit: "$(SRCROOT)/Libraries" (without quotes)
* Build Phases -> Compile sources/Link Binary With Libraries (check if all *.h files and frameworks appeared in the first place)
* General -> Identity -> Team - select your name/team name
* General -> Linked Frameworks and Libraries - check if all frameworks and libraries are in there
* Build Settings -> Code Signing - add your code sign identity and provisioning profile
* If you want to simulate app on build-in ios simulator go to Build Settings -> Architectures -> Supported Platforms and choose iOS there