﻿using UnityEngine;
using System.Collections;

public class RightLeg : MonoBehaviour
{
	
	private Touch ltouch;

	public Vector3 rightPoint;
	public bool  rightClicked = false;
	public GameObject mainCamera;
	public GameObject longLeg;

	// Cached components
	private Pause pauseScript;
	
	void  Start ()
	{
		pauseScript = mainCamera.GetComponent< Pause > ();
	}
	
	void  Update ()
	{
		if (pauseScript.isPaused == false && (Input.GetKeyDown (KeyCode.D) || rightClicked)) {
			gameObject.collider2D.enabled = !gameObject.collider2D.enabled;
			gameObject.renderer.enabled = !gameObject.renderer.enabled;
			
			longLeg.gameObject.collider2D.enabled = !longLeg.gameObject.collider2D.enabled;
			longLeg.gameObject.renderer.enabled = !longLeg.gameObject.renderer.enabled;
			
			rightClicked = false;
		}
	}
}
