﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class ScoreCounterLeft : MonoBehaviour
{
	public int scoreLeft;
	public bool  isDied;
	public AudioClip kick1;
	public AudioClip kick2;
	public AudioClip kick3;
	public AudioClip coin;

	private int audioPlayerTrack;

	void  Start ()
	{
		scoreLeft = 0;
	}
	
	void  Update ()
	{
		
	}
	
	void  OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Coin" && !isDied && other.gameObject.renderer.enabled == true) {
			scoreLeft += 1;
			audio.PlayOneShot (coin);
			other.gameObject.renderer.enabled = false;
		} else if (other.tag == "Step") {
			audioPlayerTrack = Random.Range (1, 4);
			
			switch (audioPlayerTrack) {
			case 1: 
				audio.PlayOneShot (kick1);
				break;
			case 2:
				audio.PlayOneShot (kick2);
				break;
			case 3:
				audio.PlayOneShot (kick3);
				break;
			}
		}
	}
}
