﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour
{
	public GameObject LeftLegLong;
	public GameObject RightLegLong;
	public GUISkin pauseSkin;
	public bool  isPaused;
	public GUISkin pauseMenuSkin;
	public GUISkin diedSkinReplay;
	public GUISkin pauseSkinContinue;
	private float buttonSize;
	public int currentScore;
	public GameObject LeftLeg;
	public GameObject RightLeg;
	
	private Rect pauseBtn;

	void  Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			isPaused = true;
			Time.timeScale = 0;
		}
	}

	void Start ()
	{
		buttonSize = Screen.width / 5;
		pauseBtn = new Rect (buttonSize / 4, Screen.height / 30, buttonSize / 1.5f, buttonSize / 1.5f);
	}

	public Vector2 getPauseBtnPosition ()
	{
		Vector2 position = pauseBtn.position;
		position.y = Screen.height - Screen.height / 30;
		return position;
	}

	void  OnGUI ()
	{
		GUI.skin = pauseSkin;
		if (GUI.Button (pauseBtn, "")) {
			isPaused = true;
			Time.timeScale = 0;
		}
		pauseSkin.label.fontSize = (int)(Screen.width * 0.1f);
		GUI.Label (new Rect (buttonSize / 4 * 17, Screen.height / 45, Screen.width / 5, Screen.height / 10), (LeftLeg.GetComponent< ScoreCounterLeft > ().scoreLeft + RightLeg.GetComponent< ScoreCounterRight > ().scoreRight + LeftLegLong.GetComponent< ScoreCounterLeft > ().scoreLeft + RightLegLong.GetComponent< ScoreCounterRight > ().scoreRight).ToString ());
		if (isPaused) {
			
			currentScore = LeftLeg.GetComponent< ScoreCounterLeft > ().scoreLeft + RightLeg.GetComponent< ScoreCounterRight > ().scoreRight + LeftLegLong.GetComponent< ScoreCounterLeft > ().scoreLeft + RightLegLong.GetComponent< ScoreCounterRight > ().scoreRight;
			
			GUI.skin = pauseMenuSkin;
			pauseMenuSkin.textArea.fontSize = (int)(Screen.width * 0.25f);
			GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
			GUI.TextArea (new Rect (0, Screen.height / 2 - buttonSize * 1.7f, Screen.width, Screen.height / 4), currentScore.ToString ());
			
			if (GUI.Button (new Rect (((Screen.width / 2) - buttonSize * 2), Screen.height / 4 * 2.75f, buttonSize, buttonSize), "")) {
				Application.LoadLevel ("2.MainMenu");
				Time.timeScale = 1;            
			}
			GUI.skin = diedSkinReplay;
			if (GUI.Button (new Rect (((Screen.width / 2) - buttonSize / 2), Screen.height / 4 * 2.75f, buttonSize, buttonSize), "")) {
				Application.LoadLevel (Application.loadedLevel);
				Time.timeScale = 1;
			}
			
			GUI.skin = pauseSkinContinue;
			if (GUI.Button (new Rect (((Screen.width / 2) + buttonSize), Screen.height / 4 * 2.75f, buttonSize, buttonSize), "")) {
				Time.timeScale = 1;
				isPaused = false;
			}
		}
	}
}