﻿using UnityEngine;
using System.Collections;
using System;

public class StepLooper : MonoBehaviour
{
	
	
	public int numBGPanels;
	public GameObject[] steps;
	public GameObject[] coins;
	public GameObject[] spikes;
	public GameObject step;
	public Vector3 pos;
	public float heightOfBGObject;
	public Transform coin;

	private float pipeMax = 0.5f;
	private float pipeMin = -0.3f;
	private Vector3 leftStepPosition;
	private Vector3 rightStepPosition;
	private Vector3 leftTransformPos;
	private Vector3 rightTransformPos;
	private Transform tempT;

	void  Start ()
	{
		steps = GameObject.FindGameObjectsWithTag ("StepParent");
		foreach (GameObject step in steps) {

			if (step.transform.position.y == 1)
				continue;

			leftTransformPos = step.transform.Find ("StepLeft").transform.position;
			leftTransformPos.x = -3.5f - UnityEngine.Random.Range (pipeMin, pipeMax);
			step.transform.Find ("StepLeft").transform.position = leftTransformPos;
				
			rightTransformPos = step.transform.Find ("StepRight").transform.position;
			rightTransformPos.x = 3.5f + UnityEngine.Random.Range (pipeMin, pipeMax);
			step.transform.Find ("StepRight").transform.position = rightTransformPos;
				
		}
		coins = GameObject.FindGameObjectsWithTag ("Coin");
		spikes = GameObject.FindGameObjectsWithTag ("Spikes");

		foreach (GameObject coin in coins) {
			coin.renderer.enabled = randomBool ();
		}

		int i = 0;
		foreach (GameObject spike in spikes) {
			if(i == 0 || i == 1) continue;
			spike.renderer.enabled = drawSpikes ();
			i++;
		}

	}
	
	void  OnTriggerEnter2D (Collider2D other)
	{

		if (other.tag == "StepParent") {

			heightOfBGObject = other.GetComponent<BoxCollider2D> ().size.y;

			pos = other.transform.position;
			pos.y -= heightOfBGObject * numBGPanels;
			pos.x = 0;
			other.transform.position = pos;

			tempT = other.transform.Find ("StepLeft");
			leftTransformPos = tempT.transform.position;
			leftTransformPos.x = -3.5f - UnityEngine.Random.Range (pipeMin, pipeMax);
			tempT.transform.position = leftTransformPos;

			tempT.transform.Find ("Coin").gameObject.renderer.enabled = randomBool ();
			tempT.transform.Find ("Spikes").gameObject.renderer.enabled = drawSpikes ();

			tempT = other.transform.Find ("StepRight");
			rightTransformPos = tempT.transform.position;
			rightTransformPos.x = 3.5f + UnityEngine.Random.Range (pipeMin, pipeMax);
			tempT.transform.position = rightTransformPos;

			tempT.transform.Find ("Coin").gameObject.renderer.enabled = randomBool ();
			tempT.transform.Find ("Spikes").gameObject.renderer.enabled = drawSpikes ();

		}

	}


	private bool randomBool ()
	{
		return System.Convert.ToBoolean (UnityEngine.Random.Range (0, 2));
	}

	private bool drawSpikes()
	{
		int temp = UnityEngine.Random.Range (0, 4);
		if(temp == 0) {
			return true;
		}
		return false;
	}


}
