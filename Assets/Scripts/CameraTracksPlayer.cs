﻿using UnityEngine;
using System.Collections;

public class CameraTracksPlayer : MonoBehaviour
{
	

	private Transform player;
	private float offsetY;
	private GameObject player_go;
	private Vector3 pos;

	public bool frozen = false;
	
	
	void  Start ()
	{ 
		player_go = GameObject.FindGameObjectWithTag ("Player");
		if (player_go == null) {
			Debug.LogError ("Couldn't find an object with tag 'Player'!");
			return;
		}
		player = player_go.transform;
		offsetY = transform.position.y - player.position.y + 1.5f;
	}
	
	void  Update ()
	{
		if (player != null && !frozen) {
			pos = transform.position;
			pos.y = player.position.y + offsetY;
			transform.position = pos;
		}
		// Making screen shots
		if (Input.GetKeyDown (KeyCode.P)) {
			Application.CaptureScreenshot (UnityEngine.Random.Range (100, 99999).ToString () + ".png");
		}
	}
}