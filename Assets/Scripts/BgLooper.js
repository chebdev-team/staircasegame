﻿#pragma strict

var numBGPanels : int;
var pos : Vector3;
var heightOfBGObject : float;

function Start () {

}

function OnTriggerEnter2D (other : Collider2D) {
	if(other.tag == "Background") {
		heightOfBGObject = other.GetComponent(BoxCollider2D).size.y;
		pos = other.transform.position;
		pos.y -= heightOfBGObject * numBGPanels;
		other.transform.position = pos;
	}
}