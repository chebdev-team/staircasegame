﻿using UnityEngine;
using System.Collections;

public class ScoreCounterRight : MonoBehaviour
{
	public int scoreRight;
	public bool  isDied;
	public AudioClip kick1;
	public AudioClip kick2;
	public AudioClip kick3;
	public AudioClip coin;

	private int audioPlayerTrack;

	void  Start ()
	{
		scoreRight = 0;
	}
	
	void  Update ()
	{
		
	}
	
	void  OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Coin" && !isDied && other.gameObject.renderer.enabled == true) {
			scoreRight += 1;
			audio.PlayOneShot (coin);
			other.gameObject.renderer.enabled = false;
		} else if(other.tag == "Spikes"){
			//Debug.Log("Dead");
		} else if (other.tag == "Step") {
			audioPlayerTrack = Random.Range (1, 3);
			
			switch (audioPlayerTrack) {
			case 1: 
				audio.PlayOneShot (kick1);
				break;
			case 2:
				audio.PlayOneShot (kick2);
				break;
			case 3:
				audio.PlayOneShot (kick3);
				break;
			}
		}
	}
}
