﻿using UnityEngine;
using System.Collections;

public class SkinSetter : MonoBehaviour {

	public GameObject PlayerLeftLegObj;
	public GameObject PlayerRightLegObj;
	public GameObject PlayerLeftLongLeg;
	public GameObject PlayerRightLongLeg;
	public Sprite PlayerTopBody;
	public Sprite PlayerLeftLeg;
	public Sprite PlayerRightLeg;
	public Sprite PlayerLeftLegLong;
	public Sprite PlayerRightLegLong;
	public Sprite PlayerTopBody2;
	public Sprite PlayerLeftLeg2;
	public Sprite PlayerRightLeg2;
	public Sprite PlayerLeftLegLong2;
	public Sprite PlayerRightLegLong2;

	// Use this for initialization
	void Start () {

		if (PlayerPrefs.GetInt ("Skin") == 1 || PlayerPrefs.HasKey("Skin") == false) {
			this.GetComponent<SpriteRenderer>().sprite = PlayerTopBody;
			PlayerLeftLegObj.GetComponent<SpriteRenderer>().sprite = PlayerLeftLeg;
			PlayerRightLegObj.GetComponent<SpriteRenderer>().sprite = PlayerRightLeg;
			PlayerLeftLongLeg.GetComponent<SpriteRenderer>().sprite = PlayerLeftLegLong;
			PlayerRightLongLeg.GetComponent<SpriteRenderer>().sprite = PlayerRightLegLong;
		} else if (PlayerPrefs.GetInt ("Skin") == 2) {
			this.GetComponent<SpriteRenderer>().sprite = PlayerTopBody2;
			PlayerLeftLegObj.GetComponent<SpriteRenderer>().sprite = PlayerLeftLeg2;
			PlayerRightLegObj.GetComponent<SpriteRenderer>().sprite = PlayerRightLeg2;
			PlayerLeftLongLeg.GetComponent<SpriteRenderer>().sprite = PlayerLeftLegLong2;
			PlayerRightLongLeg.GetComponent<SpriteRenderer>().sprite = PlayerRightLegLong2;
		}
	}
}
