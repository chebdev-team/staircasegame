﻿using UnityEngine;
using System.Collections;

public class SpikesCollision : MonoBehaviour
{

	private GameObject border;
	private DieScript die;
	private float explodeTimerStart = 0f;
	private GameObject player;

	public AudioClip explosion;  
	public GameObject explosionPrefab;
	public bool exploded = false;
	public GameObject mainCamera;

	void  OnTriggerEnter2D (Collider2D other)
	{

		if (other.tag == "Player" && gameObject.renderer.enabled == true) {
			explodeTimerStart = Time.time;
			exploded = true;
			audio.PlayOneShot (explosion);

			if (other.gameObject.name == "PlayerTopBody") {
				player = other.gameObject;
			} else {
				player = other.transform.parent.gameObject;
			}
			player.rigidbody2D.isKinematic = true;
			Instantiate (explosionPrefab, transform.position, transform.rotation);
		}
	}

	void Start ()
	{
		border = GameObject.Find ("BorderLeft");
		die = border.GetComponent<DieScript> ();
		mainCamera = GameObject.Find ("Main Camera");
	}

	void Update ()
	{
		if (exploded) {
			gameObject.renderer.enabled = false;
			if (Time.time >= explodeTimerStart + 0.8f && explodeTimerStart != 0f) {
				die.isDied = true;
				exploded = false;
				mainCamera.GetComponent < AdBanner > ().bannerView.Show ();
			}
		}
	}
	
}
