﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;

public class Borders : MonoBehaviour
{
	public Camera mainCam;
	private float screenRatio;
	//Reference the colliders we are going to adjust
	public GameObject leftWall;
	public GameObject rightWall;
	private float screenHeight = Screen.height;
	private float screenWidth = Screen.width;

	void  Start ()
	{ 
		Time.timeScale = 1; 
		screenRatio = screenHeight / screenWidth;

		float margin = Mathf.Abs ((Screen.width - (Screen.height / 1.7f)) / 2);

		Vector3 pos = leftWall.transform.position;
		pos.x = mainCam.ScreenToWorldPoint (new Vector3 (margin, 0f, 0f)).x;
		leftWall.transform.position = pos;

		pos = rightWall.transform.position;
		pos.x = mainCam.ScreenToWorldPoint (new Vector3 (Screen.width - margin, 0f, 0f)).x;
		rightWall.transform.position = pos;
	}
}