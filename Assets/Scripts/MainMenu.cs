﻿// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.

using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;
using System;

public class MainMenu : MonoBehaviour
{
	
	
	public float buttonSize;
	public bool  isQuite;
	public GUISkin mainMenuSkin;
	public GUISkin settingsSkin;
	public GUISkin settingsDisabledSkin;
	public GUISkin dialogQuit;
	public GUISkin aboutMenu;
	public GUISkin infoSkin;
	private bool  isAbout = false;

	private string leaderboardID = "";
	private string iosLeaderBoardID = "101";
	private string androidsLeaderBoardID = "CgkI6benma4eEAIQAQ";
	private string urlString;

	public AudioClip mainMenu;
	
	void  Start ()
	{

		#if UNITY_IPHONE
		leaderboardID = iosLeaderBoardID;
		urlString = "https://itunes.apple.com/us/app/fatal-staircase/id906740906?l=ru&ls=1&mt=8";
		#endif

		#if UNITY_ANDROID
		GooglePlayGames.PlayGamesPlatform.Activate ();
		leaderboardID = androidsLeaderBoardID;
		urlString = "market://details?id=com.chebdev.fatalstaircase";
		#endif

		audio.clip = mainMenu;
		audio.Play ();
		audio.loop = true;

	}

	void  Update ()
	{
		if (PlayerPrefs.GetInt ("SoundState") == 1) {
			AudioListener.volume = 100;
		} else {
			AudioListener.volume = 0;
		}
		// Making screen shots
		if (Input.GetKeyDown (KeyCode.P)) {
			Application.CaptureScreenshot (UnityEngine.Random.Range (100, 999).ToString () + ".png");
		}
	}
	
	void  OnGUI ()
	{
		
		buttonSize = Screen.width / 2.5f;
		GUI.skin = mainMenuSkin;
		GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
		mainMenuSkin.button.fontSize = (int)(Screen.width * 0.06f);
		if (!isQuite) {

			if (GUI.Button (new Rect ((Screen.width / 2 - 0.5f * buttonSize), (Screen.height / 10 * 6.3f + Screen.height / 20), buttonSize, Screen.height / 14), "Play")) {
				Application.LoadLevel ("2.5PersSelect");
			}

			// Leaderboard button
			if (GUI.Button (new Rect ((Screen.width / 2 - 0.5f * buttonSize), (Screen.height / 10 * 7.2f + Screen.height / 20), buttonSize, Screen.height / 14), "Leaders")) {
				if (Social.localUser.authenticated) {
					Social.ReportScore (PlayerPrefs.GetInt ("Score"), leaderboardID, (bool posted) => {
						if (posted) {

							#if UNITY_ANDROID
							((PlayGamesPlatform)Social.Active).ShowLeaderboardUI (leaderboardID);
							#endif

							#if UNITY_IPHONE
							UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform.ShowLeaderboardUI (leaderboardID, TimeScope.AllTime);
							#endif

						}
					});
				} else {
					Debug.Log ("Authenticating");
					Social.localUser.Authenticate ((bool success) => {
						if (success) {
							if (Social.localUser.authenticated) {
								Social.ReportScore (PlayerPrefs.GetInt ("Score"), leaderboardID, (bool posted) => {
									if (posted) {

										#if UNITY_ANDROID
										((PlayGamesPlatform)Social.Active).ShowLeaderboardUI (leaderboardID);
										#endif
										
										#if UNITY_IPHONE
										UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform.ShowLeaderboardUI (leaderboardID, TimeScope.AllTime);
										#endif

									}
								});
							} else {
								Debug.Log ("Can't post score, no auth");
							}
						}
					});
				}
			}
			
			if (GUI.Button (new Rect ((Screen.width / 2 - 0.5f * buttonSize), (Screen.height / 10 * 8.1f + Screen.height / 20), buttonSize, Screen.height / 14), "Exit")) {
				isQuite = true;
			}
			
			
			if (AudioListener.volume != 0) {
				GUI.skin = settingsSkin;

				if (!isAbout && GUI.Button (new Rect ((Screen.width / 20 * 1), (Screen.height / 12) * 11, Screen.width / 9, Screen.width / 9), "")) {
					PlayerPrefs.SetInt ("SoundState", 0);
					
				}
			} else {
				GUI.skin = settingsDisabledSkin;
				if (!isAbout && GUI.Button (new Rect ((Screen.width / 20 * 1), (Screen.height / 12) * 11, Screen.width / 9, Screen.width / 9), "")) {
					PlayerPrefs.SetInt ("SoundState", 1);
					
				}
			}
			
		}
		
		GUI.skin = infoSkin;
		if (GUI.Button (new Rect ((Screen.width / 20 * 17), (Screen.height / 12) * 11, Screen.width / 9, Screen.width / 9), "")) {
			isAbout = true;
		}
		
		if (isAbout) {
			GUI.skin = aboutMenu;
			aboutMenu.textArea.fontSize = (int)(Screen.width * 0.04f);
			GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
			if (GUI.Button (new Rect ((Screen.width / 20 * 1), Screen.height / 4 * 3.5f, Screen.width / 6, Screen.width / 6), "")) {
				isAbout = false;
			}
			
			
			GUI.TextArea (new Rect (Screen.width / 5, Screen.height / 5 * 2, Screen.width / 5 * 3, Screen.height / 5 * 3), "\n Created by ChebDev Studio. \n \n Character sprites from: \n http://vectorcharacters.net/ ", 10000);
		}
		
		if (isQuite) {
			GUI.skin = dialogQuit;
			dialogQuit.button.fontSize = (int)(Screen.width * 0.06f);
			GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
			
			if (GUI.Button (new Rect ((Screen.width / 2 - 0.5f * buttonSize), (Screen.height / 10 * 6.3f + Screen.height / 20), buttonSize, Screen.height / 14), "YES")) {
				Application.Quit ();
				
			}
			if (GUI.Button (new Rect ((Screen.width / 2 - 0.5f * buttonSize), (Screen.height / 10 * 7.2f + Screen.height / 20), buttonSize, Screen.height / 14), "NO")) {
				isQuite = false;
				Application.LoadLevel ("2.MainMenu");
			}

			if (GUI.Button (new Rect ((Screen.width / 2 - 0.5f * buttonSize), (Screen.height / 10 * 8.1f + Screen.height / 20), buttonSize, Screen.height / 14), "Rate us")) {
//				string urlString = "market://details?id=" + "com.chebdev.fatalstaircase";
				Application.OpenURL (urlString);
			}
		}
	}

	IEnumerator checkInternetConnection (Action<bool> action)
	{
		WWW www = new WWW ("http://google.com");
		yield return www;
		if (www.error != null) {
			action (false);
		} else {
			action (true);
		}
	} 
}