﻿using UnityEngine;
using System.Collections;

public class LeftLeg : MonoBehaviour
{
	
	private Touch rtouch;

	public Vector3 leftPoint;
	public bool  leftClicked = false;
	public GameObject mainCamera;
	public GameObject longLeg;

	// Cached components
	private Pause pauseScript;
	
	void  Start ()
	{
		pauseScript = mainCamera.GetComponent< Pause > ();
	}
	
	void  Update ()
	{
		
		if (pauseScript.isPaused == false && (Input.GetKeyDown (KeyCode.A) || leftClicked)) {

			gameObject.collider2D.enabled = !gameObject.collider2D.enabled;
			gameObject.renderer.enabled = !gameObject.renderer.enabled;

			longLeg.gameObject.collider2D.enabled = !longLeg.gameObject.collider2D.enabled;
			longLeg.gameObject.renderer.enabled = !longLeg.gameObject.renderer.enabled;

			leftClicked = false;

		}
	}
}