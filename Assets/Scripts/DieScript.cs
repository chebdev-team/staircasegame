// Converted from UnityScript to C# at http://www.M2H.nl/files/js_to_c.php - by Mike Hergaarden
// Do test the code! You usually need to change a few small bits.
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class DieScript : MonoBehaviour
{
	public bool  isDied = false;
	public GUISkin diedSkin;
	public GUISkin twitterSkin;
	public GUISkin fbSkin;
	public GUISkin diedSkinReplay;
	private float buttonSize;
	public int scoreLeft;
	public int scoreRight;
	public int scoreTotal;
	public GameObject LeftLeg;
	public GameObject RightLeg;
	public AudioClip explosion; 
	public bool isAudioPlayed = false;
	public  GameObject MainCamera;
	public  GameObject LeftLegLong;
	public  GameObject RightLegLong;
	//* Twitter variables */
	private string Address = "http://twitter.com/intent/tweet";
	private string lang = "en";
	private string text;
	private string url = "https://play.google.com/store/apps/details?id=com.chebdev.fatalstaircase";
	/* END Twitter variables */
	
	//* Facebook variables */
	private string AppId = "505805912884406";
	private string ShareUrl = "http://www.facebook.com/dialog/feed";
	private string fblink = "https://play.google.com/store/apps/details?id=com.chebdev.fatalstaircase";
	private string fbpictureLink = "https://lh5.googleusercontent.com/-SvREJE0FMCI/VANAYWcwA2I/AAAAAAAAAGQ/oUYrrUhOUvU/s512-no/fatal_staircase.png";
	private string fbname;
	private string fbcaption = "Find this game on Google Play";
	private string fbdescription ;
	private string fbredirectUri = "http://www.facebook.com";
	/* END Facebook variables */

	private string leaderboardID;
	private string iosLeaderboardID = "101";
	private string androidLeaderboardID = "CgkI6benma4eEAIQAQ";


	void Start ()
	{
		text = fbname = "My best score in the Fatal Staircase " + PlayerPrefs.GetInt ("Score").ToString ();

		#if UNITY_IPHONE
		leaderboardID = iosLeaderboardID;
		url = fblink = "https://itunes.apple.com/us/app/fatal-staircase/id906740906?l=ru&ls=1&mt=8";
		fbcaption = "Find this game on iTunes";
		#endif
		
		#if UNITY_ANDROID
		leaderboardID = androidLeaderboardID;
		#endif
	}
	
	//Twitter method
	public void  TwiterShare (string text, string url, string lang)
	{
		Application.OpenURL (Address +
			"?text=" + WWW.EscapeURL (text) + 
			"&url=" + WWW.EscapeURL (url) +
			"&lang=" + WWW.EscapeURL (lang));
	}
	
	//Facebook method
	public void  fbShare (string fblink, string fbpictureLink, string fbname, string fbcaption, string fbdescription, string fbredirectUri)
	{	
		Application.OpenURL (ShareUrl +
			"?app_id=" + AppId +
			"&link=" + WWW.EscapeURL (fblink) +
			"&picture=" + WWW.EscapeURL (fbpictureLink) +
			"&name=" + WWW.EscapeURL (fbname) +
			"&caption=" + WWW.EscapeURL (fbcaption) +
			"&description=" + WWW.EscapeURL (fbdescription) +
			"&redirect_uri=" + WWW.EscapeURL (fbredirectUri));
	}
	
	void  OnGUI ()
	{
		diedSkin.label.fontSize = (int)(Screen.width * 0.25f);
		if (isDied) {
			LeftLeg.GetComponent < ScoreCounterLeft > ().isDied = true;
			RightLeg.GetComponent < ScoreCounterRight > ().isDied = true;
			scoreLeft = LeftLeg.GetComponent< ScoreCounterLeft > ().scoreLeft;
			scoreRight = RightLeg.GetComponent< ScoreCounterRight > ().scoreRight;
			scoreTotal = scoreLeft + scoreRight + LeftLegLong.GetComponent< ScoreCounterLeft > ().scoreLeft + RightLegLong.GetComponent< ScoreCounterRight > ().scoreRight;

			if (PlayerPrefs.GetInt ("Score") < scoreTotal) {
				PlayerPrefs.SetInt ("Score", scoreTotal);

				// Send score to Google Leaderboard if user is authenticated
				if (Social.localUser.authenticated) {
					Social.ReportScore (PlayerPrefs.GetInt ("Score"), leaderboardID, (bool posted) => {

					});
				} else {
					Debug.Log ("Can't post score, no auth");
				}
			}

			GUI.skin = diedSkin;
			buttonSize = Screen.width / 6;
			GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
			GUI.Label (new Rect (0, Screen.height / 8 * 2, Screen.width, Screen.height / 6), scoreTotal.ToString ());
			

			if (GUI.Button (new Rect (buttonSize, Screen.height / 2 + 1.5f * buttonSize, buttonSize, buttonSize), "")) {
				Application.LoadLevel ("2.MainMenu");
				MainCamera.GetComponent < AdBanner > ().bannerView.Hide ();
			}
			
			GUI.skin = diedSkinReplay;
			if (GUI.Button (new Rect (4 * buttonSize, Screen.height / 2 + 1.5f * buttonSize, buttonSize, buttonSize), "")) {
				Application.LoadLevel (Application.loadedLevel);
				MainCamera.GetComponent < AdBanner > ().bannerView.Hide ();

			}
			twitterSkin.label.fontSize = (int)(Screen.width * 0.15f);
			GUI.skin = twitterSkin;
			text = "My high score in the Fatal Staircase game is " + PlayerPrefs.GetInt ("Score").ToString ();
			GUI.Label (new Rect (0, Screen.height / 8 * 4, Screen.width, Screen.height / 6), PlayerPrefs.GetInt ("Score").ToString ());
			if (GUI.Button (new Rect ((Screen.width / 10 * 7), (Screen.height / 11.25f) * 10, Screen.width / 10, Screen.width / 10), "")) {
				TwiterShare (text, url, lang);
			}
			fbdescription = "My high score in the Fatal Staircase game is " + PlayerPrefs.GetInt ("Score").ToString ();
			GUI.skin = fbSkin;
			if (GUI.Button (new Rect ((Screen.width / 10 * 8.5f), (Screen.height / 11.25f) * 10, Screen.width / 10, Screen.width / 10), "")) {
				fbShare (fblink, fbpictureLink, fbname, fbcaption, fbdescription, fbredirectUri);
			}
		}
	}
}
