﻿using UnityEngine;
using System.Collections;

public class SelectPers : MonoBehaviour
{

	public GUISkin selectPers;
	public GUISkin selectGirl;
	public GUISkin selectBoy;
	public GUISkin selectButtonGreen;
	public GUISkin selectButtonGray;
	public GUISkin selectButtonPlay;


	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.P)) {
			Application.CaptureScreenshot (UnityEngine.Random.Range (100, 999).ToString () + ".png");
		}
	}

	void  OnGUI ()
	{
		GUI.skin = selectPers;
		GUI.Box (new Rect (0, 0, Screen.width, Screen.height), "");
		if (GUI.Button (new Rect ((Screen.width / 20 * 2), Screen.height / 4 * 3.5f, Screen.width / 6, Screen.width / 6), "")) {
			Application.LoadLevel ("2.MainMenu");
		}

		GUI.skin = selectButtonPlay;
		if (GUI.Button (new Rect ((Screen.width / 20 * 15), Screen.height / 4 * 3.5f, Screen.width / 6, Screen.width / 6), "")) {
			Application.LoadLevel ("3.GameScene");
		}


		if (PlayerPrefs.GetInt ("Skin") == 1 || PlayerPrefs.HasKey ("Skin") == false) {
			GUI.skin = selectButtonGreen;
			if (GUI.Button (new Rect ((Screen.width / 12), Screen.height / 11 * 8, Screen.width / 3, Screen.height / 14), "")) {
				PlayerPrefs.SetInt ("Skin", 1);
			}

			GUI.skin = selectButtonGray;
			if (GUI.Button (new Rect ((Screen.width / 12 * 7), Screen.height / 11 * 8, Screen.width / 3, Screen.height / 14), "")) {
				PlayerPrefs.SetInt ("Skin", 2);
			}
		} else if (PlayerPrefs.GetInt ("Skin") == 2) {
			GUI.skin = selectButtonGray;
			if (GUI.Button (new Rect ((Screen.width / 12), Screen.height / 11 * 8, Screen.width / 3, Screen.height / 14), "")) {
				PlayerPrefs.SetInt ("Skin", 1);
			}
			
			GUI.skin = selectButtonGreen;
			if (GUI.Button (new Rect ((Screen.width / 12 * 7), Screen.height / 11 * 8, Screen.width / 3, Screen.height / 14), "")) {
				PlayerPrefs.SetInt ("Skin", 2);
			}
		}

	}

}