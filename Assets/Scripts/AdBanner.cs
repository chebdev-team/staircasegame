﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class AdBanner : MonoBehaviour
{
	public  BannerView bannerView;
	private string admobID;

	// Use this for initialization
	void Start ()
	{
		#if UNITY_IPHONE
		admobID = "ca-app-pub-7003710056124472/7087345948";
		#endif
		
		#if UNITY_ANDROID
		admobID = "ca-app-pub-7003710056124472/4290171146";
		#endif

		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView (admobID, AdSize.Banner, AdPosition.Top);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder ().Build ();
		// Load the banner with the request.
		bannerView.LoadAd (request);
		bannerView.Hide ();
	}

}
