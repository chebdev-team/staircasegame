﻿#pragma strict

var player : Transform;
var offsetY : float;
var player_go : GameObject;
var pos : Vector3;


function Start () {
	player_go = GameObject.FindGameObjectWithTag("Player");
	if(player_go == null) {
		Debug.LogError("Couldn't find an object with tag 'Player'!");
		return;
	}
	player = player_go.transform;
	offsetY = transform.position.y - player.position.y + 1.5f;
}

function Update () {
	if(player != null) {
		pos = transform.position;
		pos.y = player.position.y + offsetY;
		transform.position = pos;
	}
	// Making screen shots
	if (Input.GetKeyDown (KeyCode.P)) {
		Application.CaptureScreenshot (UnityEngine.Random.Range (100, 999).ToString () + ".png");
	}
}

