﻿using UnityEngine;
using System.Collections;

public class Speedometer : MonoBehaviour
{
	public GameObject leftBorder;
	public GameObject rightBorder;
	public GameObject LeftLeg;
	public GameObject RightLeg;
	public int fintScore = 8;
	public int fintDegree = 540;
	public AudioClip scoreClip;
	public AudioClip scoreComboClip;
	public AudioClip coin;
	public AudioClip scream;
	public GameObject score2PointPrefab;
	public GameObject scoreComboPrefab;
	public GUISkin speedYellow;
	public GUISkin speedRed;
	public GUISkin speedProgressBar;
	public GUISkin speedGreen;
	public GameObject PlayerFire;

	public int speedLimit = 30;

	private GameObject scoreCombo;
	private float speed;
	private bool clockwise;
	private int currentAngle = 0;
	private int previousAngle = 0;
	private int total = 0;
	private bool previousClockwise;
	private GameObject score2Point;
	private Touch rtouch;

	//Cached components
	private DieScript leftDieScript;
	private DieScript rightDieScript;
	private ScoreCounterLeft scoreCounter;
	private LeftLeg lleg;
	private RightLeg rleg;

	public GameObject mainCamera;
	private Pause pause;
	private Vector2 pauseBtnPosition;
	private Vector3 touchPos;
	private Vector3 playerPos;
	private CameraTracksPlayer camera;
	private float feltTime = 0f;


	void Start ()
	{
		leftDieScript = leftBorder.GetComponent< DieScript > ();
		rightDieScript = rightBorder.GetComponent< DieScript > ();
		scoreCounter = LeftLeg.GetComponent< ScoreCounterLeft > ();
		lleg = LeftLeg.GetComponent<LeftLeg> ();
		rleg = RightLeg.GetComponent<RightLeg> ();

		pause = mainCamera.GetComponent<Pause> ();
		camera = mainCamera.GetComponent<CameraTracksPlayer> ();

	}

	void  Update ()
	{

		speed = rigidbody2D.velocity.magnitude;

		if (feltTime == 0f && speed > speedLimit) {
			camera.frozen = true;
			feltTime = Time.time;
			audio.PlayOneShot (scream);
		}

		if (feltTime != 0f && Time.time >= (feltTime + 1f) && leftDieScript.isDied == false) {
			leftDieScript.isDied = true; 
			mainCamera.GetComponent < AdBanner > ().bannerView.Show ();
		}

		if (leftDieScript.isDied == true || rightDieScript.isDied == true) {
			Time.timeScale = 0;
		}

		currentAngle = (int)transform.eulerAngles.z;

		if (currentAngle - previousAngle != 0) {
			if ((currentAngle > previousAngle && currentAngle - previousAngle < 180) || (currentAngle - previousAngle < -180)) {
				clockwise = false;
			} else {
				clockwise = true;
			}

			if (clockwise != previousClockwise) {
				total = 0;
			}
			
			if (clockwise) {
				if (currentAngle - previousAngle > 180) {
					total += previousAngle + (360 - currentAngle);
				} else {
					total += previousAngle - currentAngle;
				}
			} else {
				if (previousAngle - currentAngle > 180) {
					total += currentAngle + (360 - previousAngle);
				} else {
					total += currentAngle - previousAngle;
				}
			}

			previousAngle = currentAngle;
			previousClockwise = clockwise;
		}


		if (total >= fintDegree) {
			total = 0;
			scoreCounter.scoreLeft += fintScore;
			scoreCombo = Instantiate (scoreComboPrefab, transform.position, Quaternion.identity) as GameObject;
			Destroy (scoreCombo, 1);
			audio.PlayOneShot (scoreComboClip);
		}

		if (!pause.isPaused && speed < speedLimit) {
			for (int i= 0; i < Input.touchCount; ++i) {
				rtouch = Input.GetTouch (i);
				if (rtouch.phase == TouchPhase.Began) {
					touchPos = Camera.main.ScreenToWorldPoint (rtouch.position);
					playerPos = transform.position;
					pauseBtnPosition = Camera.main.ScreenToWorldPoint (pause.getPauseBtnPosition ());
					
					if (Mathf.Abs (touchPos.x - playerPos.x) <= 1 && Mathf.Abs (touchPos.y - playerPos.y) <= 1) {
						if (playerPos.x > 0) {
							rigidbody2D.AddForce (new Vector2 (-70, 0));
						} else {
							rigidbody2D.AddForce (new Vector2 (70, 0));
						}
					} else if (Mathf.Abs (touchPos.x - pauseBtnPosition.x) <= 1 && Mathf.Abs (touchPos.y - pauseBtnPosition.y) <= 1) {
						break;
					} else {
						if (rtouch.position.x < (Screen.width / 2)) {
							lleg.leftClicked = true;
						} else {
							rleg.rightClicked = true;
						}
					}
				}
			}
		}

		if (Input.GetKeyDown (KeyCode.S)) {
			if (transform.position.x > 0) {
				rigidbody2D.AddForce (new Vector2 (-70, 0));
			} else {
				rigidbody2D.AddForce (new Vector2 (70, 0));
			}
		}

		if (Input.GetKeyDown (KeyCode.F)) {
			pauseBtnPosition = Camera.main.ScreenToWorldPoint (pause.getPauseBtnPosition ());
			Debug.Log (pauseBtnPosition);
		}
	}

	void OnMouseDown ()
	{
		if (transform.position.x > 0) {
			rigidbody2D.AddForce (new Vector2 (-70, 0));
		} else {
			rigidbody2D.AddForce (new Vector2 (70, 0));
		}
	}

	void OnGUI ()
	{
		if (LeftLeg.GetComponent < ScoreCounterLeft > ().isDied == false) {
			//draw the background:
			GUI.skin = speedProgressBar;
			GUI.Box (new Rect (0, 0, Screen.width, Screen.height / 50), "");
	
			if (speed < 20) {
				GUI.skin = speedGreen;
				//draw the filled-in part:
				GUI.Box (new Rect (0, 0, Screen.width / speedLimit * speed, Screen.height / 50), "");
			} else if (speed > 20 && speed < 30) {
				GUI.skin = speedYellow;
				//draw the filled-in part:
				GUI.Box (new Rect (0, 0, Screen.width / speedLimit * speed, Screen.height / 50), "");
			} else {
				GUI.skin = speedRed;
				//draw the filled-in part:
				GUI.Box (new Rect (0, 0, Screen.width / speedLimit * speed, Screen.height / 50), "");
			}
		}
	}

	void  OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Coin" && scoreCounter.isDied == false && other.gameObject.renderer.enabled == true) {
			scoreCounter.scoreLeft += 1;
			audio.PlayOneShot (coin);
			other.renderer.enabled = false;
			other.gameObject.renderer.enabled = false;
		}
	}
}